import Head from 'next/head'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <section className={styles.headContainer}>
      <Head>
        <title>Barco Hacker - Tecnologia e inovação pelos rios da Amazônia</title>
        <link rel="icon" href="/favicon-.ico" />
      </Head>

      <main className={styles.main}>
        <div className={styles.boat}>
          <img src="/logo.png" alt="Logo Barco Hacker" className={styles.logo} />
        </div>
      </main>

    </section>
  )
}
